<!-- fichier src/Template/Users/add.ctp -->

<h1>Créer un compte</h1>

<?= $this->Form->create($user) ?>
    <?= $this->Form->control('pseudo');?>
    <?= $this->Form->control('password', ['label' => 'Mot de passe']);?>
    <?= $this->Form->button('Envoyer') ?>
<?= $this->Form->end() ?>
