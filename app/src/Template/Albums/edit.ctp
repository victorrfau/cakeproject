<?php //file src/Templates/Movies/edit.ctp ?>

<?= $this->Form->create($albums) ?>
<h1>Modifier l'album <?php echo ($albums->title); ?></h1>
<?= $this->Form->control('title', ['label' => 'Titre de l\'album']) ?>
<?= $this->Form->control('genre', ['label' => 'genre de l\'album']) ?>
<?= $this->Form->control('spotify', ['label' => 'Id spotify']) ?>

<?= $this->Form->button('Modifier') ?>
<?= $this->Form->end() ?>
