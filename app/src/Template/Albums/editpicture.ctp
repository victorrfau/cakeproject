<?php
//var_dump($this)
//file src/Templates/Movies/editpicture.ctp ?>

<?= $this->Form->create($albums, ['enctype' => 'multipart/form-data']) ?>
    <h1>Modification de la pochette de <?= $albums->title ?></h1>

<?=  $this->Form->control('cover', ['type' => 'file', 'label' => 'Cover']) ?>
    <figure>
        <?php //si on a l'image, on l'affiche; sinon, on met une image par defaut
        if (!empty($albums->cover)) { ?>
            <?= $this->Html->image( '../data/pochettes/'.$albums->cover, ['alt' => 'Cover de : '.$albums->title ] ) ?>
        <?php }else { ?>
            <?= $this->Html->image('defaut.jpg', ['alt' => 'Visuel non disponible' ] ) ?>
        <?php } ?>
        <figcaption>
            Image actuelle
        </figcaption>
    </figure>

<?= $this->Form->button('Modifier') ?>
<?= $this->Form->end() ?>
