<h1>hey</h1>
<?php
;
//$table = $this;
//$id = $this->passedArgs[0];
//echo $id;
?>

<span class="label">Titre :</span>
<p><?php echo ($albums->title); ?></p>
<span class="label">Pochette :</span>
<p><?php echo ($albums->cover); ?></p>
<figure>
    <?php //si on a l'image, on l'affiche; sinon, on met une image par defaut
    if (empty($albums->cover)) { ?>
        <?= $this->Html->image('defaut.jpg', ['alt' => 'Visuel non disponible' ] ) ?>
    <?php }else { ?>
        <?= $this->Html->image( '../data/pochettes/'.$albums->cover, ['alt' => 'Affiche de : '.$albums->titre ] ) ?>
    <?php } ?>
    <figcaption>
        <?= $this->Html->link('Modifier la photo de profile', ['action' => 'editpicture', $albums->id], ['class' => "button"]) ?>
    </figcaption>
</figure>
<span class="label">genre :</span>
<p><?php echo ($albums->genre); ?></p>
<span class="label">spotify :</span>
<p><?php echo ($albums->spotify); ?></p>

<iframe src="https://open.spotify.com/embed/album/<?php echo ($albums->spotify); ?>" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>

<p>
    <?= $this->Html->link('Modifier', ['action' => 'edit', $albums->id], ['class' => "button"]) ?>
</p>
