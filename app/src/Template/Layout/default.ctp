<?php
$cakeDescription = 'Gestionnaire de films';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $this->fetch('title') ?> - 
        <?= $cakeDescription ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('reset.css') ?>
    <?= $this->Html->css('main.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
    <header>
        <h1><?= $this->Html->link('Artists', ['controller' => 'Artists', 'action' => 'index']) ?></h1>
        <nav>
            <?= $this->Html->link('Liste des Artists', ['controller' => 'Artists', 'action' => 'index'], ['class' => ($this->templatePath == 'Artists' && $this->template == 'index') ? 'active':'']) ?>
            <?= $this->Html->link('Ajouter un Artists', ['controller' => 'Artists', 'action' => 'add'], ['class' => ($this->templatePath == 'Artists' && $this->template == 'add') ? 'active':''])?>
            <?= $this->Html->link('Listes des requests', ['controller' => 'requests', 'action' => 'index'], ['class' => ($this->templatePath == 'Requests' && $this->template == 'index') ? 'active':''])?>
            <?= $this->Html->link('Faire une request', ['controller' => 'requests', 'action' => 'add'], ['class' => ($this->templatePath == 'Requests' && $this->template == 'add') ? 'active':''])?>



            <?= $this->Html->link('Liste des utilisateurs', ['controller' => 'users', 'action' => 'index'], ['class' => ($this->templatePath == 'Users' && $this->template == 'index') ? 'active':'']) ?>
            <?php
            if($auth->user()){
                echo $this->html->link("se déconecter", ['action' => 'logout']);
            }else{
                echo $this->Html->link('Créer un compte', ['controller' => 'users', 'action' => 'add'], ['class' => ($this->templatePath == 'Users' && $this->template == 'add') ? 'active':'']);


                echo $this->Html->link('se connecter', ['controller' => 'users', 'action' => 'login'], ['class' => ($this->templatePath == 'Users' && $this->template == 'login') ? 'active':'']);
            }
            ?>

        </nav>
        <?php
            if($auth->user()){
                echo '<p class="hello">Bonjour'.$auth->user('pseudo').'</p>';
            }
        ?>
    </header>

    <main>
        <!-- Affiche les messages pour l'utilisateur (et les vide de la memoire) -->
        <div class="messages">
            <?= $this->Flash->render() ?>
        </div>
        <?php //var_dump($auth->user())?>
        <!-- affiche le contenu de cette page-->
        <?=$this->fetch('content') ?>
    </main>
    
</body>
</html>
