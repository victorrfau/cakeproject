<!-- fichier src/Template/Users/index.ctp -->

<h1>Liste des requests</h1>
<?php

if($requests->count() == 0){
    echo '<p>Il n\'y a aucun utilisateur.</p>';
}else{
    echo '<ul class="list">';
    foreach ($requests as $u) { ?>
        <li>
            <?= $u->pseudo ?>
            <?= $u->artistname ?>
            <?= $u->albumtitle ?>
        </li>
    <?php }
    echo '</ul>';
}
?>
