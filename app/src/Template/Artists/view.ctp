<?php //file src/Tempaltes/Movies/view.ctp
//var_dump($one);
//echo $one;

$b = 0;
?>


<h1><?= $one->psodonym?></h1>
<figure>
    <?php //si on a l'image, on l'affiche; sinon, on met une image par defaut
    if (empty($one->poster)) { ?>
        <?= $this->Html->image( '../data/artistes/'.$one->picture, ['alt' => 'Affiche de : '.$one->psodonym ] ) ?>
    <?php }else { ?>
        <?= $this->Html->image('defaut.jpg', ['alt' => 'Visuel non disponible' ] ) ?>
    <?php } ?>
    <figcaption>
        <?= $this->Html->link('Modifier la photo de profile', ['action' => 'editpicture', $one->id], ['class' => "button"]) ?>
    </figcaption>
</figure>
    <iframe src="https://open.spotify.com/follow/1/?uri=spotify:artist:<?php echo ($one->spotify); ?>&size=detail&theme=light" width="300" height="56" scrolling="no" frameborder="0" style="border:none; overflow:hidden;" allowtransparency="true"></iframe>


<p>
    <span class="label">Année de commencement :</span>
    <?= $one->debut ?>
</p>
<p>
    <span class="label">Pays d'origin :</span>
    <?= $one->country ?>
</p>
<p>
    <?= $this->Html->link('Modifier', ['action' => 'edit', $one->id], ['class' => "button"]) ?>
</p>

<h2>Albums</h2>
    <?php foreach ($one->albums as $key => $value){ ?>
        <p class="infos"> <?= $value->created->i18nFormat('dd/mm/yyyy HH:MM:SS')?> : <?= $this->Html->link($value->title, ['controller' => 'Albums', 'action'=>'viewAlbum', $value->id]) ?>. <?= $value->cover?></p>
    <?php }; ?>

<h2>Favorie</h2>


<?= $this->Html->link('Modifier', ['action' => 'add', $one->id], ['class' => "button"]) ?>


<?php
if (!empty($one->bookmarks)){
    //var_dump($one);
    foreach ($one->bookmarks as $key ){
        ?>
        <p class="infos"><?= $key->user->pseudo ?></p>
        <?php
        $b = $b + 1;
    };
    $result = $b;
    echo $result . ' Personnes aime cet artists';
}else{
    echo 'Personne n aime ce truc';
}
?>
