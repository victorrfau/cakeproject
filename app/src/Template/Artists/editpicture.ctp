<?php
?>
<?= $this->Form->create($artists, ['enctype' => 'multipart/form-data']) ?>
    <h1>Modification de l'affiche de : <?= $artists->title ?></h1>

<?=  $this->Form->control('picture', ['type' => 'file', 'label' => 'Affiche']) ?>
    <figure>
        <?php //si on a l'image, on l'affiche; sinon, on met une image par defaut
        if (!empty($artists->picture)) { ?>
            <?= $this->Html->image( '../data/artistes/'.$artists->picture, ['alt' => 'Affiche de : '.$artists->title ] ) ?>
        <?php }else { ?>
            <?= $this->Html->image('defaut.jpg', ['alt' => 'Visuel non disponible' ] ) ?>
        <?php } ?>
        <figcaption>
            Image actuelle
        </figcaption>
    </figure>

<?= $this->Form->button('Modifier') ?>
<?= $this->Form->end() ?>
