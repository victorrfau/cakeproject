<h1>Liste des Artistes</h1>

<p>Il y a <?= $artists->count() ?> Artiste<?= ($artists->count()>1) ? 's' : '' ?></p>

<table>
    <thead>
    <tr>
        <th>Artiste</th>
        <th>Pays</th>
        <th>Lien spotify</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($artists as $one) : ?>
        <tr>
            <td><?= $this->Html->link($one->psodonym, ['action'=>'view', $one->id]) ?></td>
            <td><?= $one->country ?></td>
            <td><?= $one->spotify?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
