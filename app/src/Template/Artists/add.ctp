<?php //file src/Templates/Movies/add.ctp ?>

<?= $this->Form->create($new, ['enctype' => 'multipart/form-data']) ?>
    <h1>Ajouter un artiste</h1>
<?= $this->Form->control('psodonym', ['label' => 'nom de l\'artiste']) ?>
<?= $this->Form->control('debut', ['label' => 'debut']) ?>
<?= $this->Form->control('country', ['label' => 'pays d\'origin']) ?>
<?= $this->Form->control('spotify', ['label' => 'nom spotify']) ?>

<?=  $this->Form->control('picture', ['type' => 'file', 'label' => 'Affiche']) ?>

<?= $this->Form->button('Ajouter') ?>
<?= $this->Form->end() ?>
