<?php // src/Controller/UsersController.php

namespace App\Controller;

class UsersController extends AppController {

    public function initialize(){
        parent::initialize();
        // Ajoute l'action 'add' de ce controller à la liste des actions autorisées sans etre connecté
        $this->Auth->allow(['add']);
    }

    public function index(){
        $users = $this->Users->find()->order('pseudo');
        $this->set(compact('users'));
    }


    public function add(){
        $user = $this->Users->newEntity();

        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success('tu es pret pour la suite.');
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error('tu es mauvais tu as raté');
        }
        $this->set(compact('user'));
    }


    public function login(){
        if ($this->request->is('post')) {
            //essaye de matcher avec un utilisateur de la base
            $user = $this->Auth->identify();
            //si on a trouve quelqu'un qui matche
            if ($user) {
                //on le memorise en session
                $this->Auth->setUser($user);
                //on redirige vers la page d'avant
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error('fais un effort, tu peux quand meme te souvenir ! ');
        }
    }

    public function logout(){
        $this->Flash->success('pour tu pars ? au plaisir de te revoir');
        $this->Auth->logout();
        return $this->redirect(['controller' =>'Artists', 'action'=>'index']);
    }


}
