<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

// todo Ajouter la fonction pour retirer un artiste des favories

class BookmarksController extends AppController{


    // cette fonction permet d'ajouter un artiste au favories.
    public function add(){

        $Book = $this->Bookmarks->newEntity();

        if ($this->request->is('post')) {
            $Book = $this->Bookmarks->patchEntity($Book, $this->request->getData());
            $Book->user_id = $this->Auth->user('id');
            $Book->artist_id = $key->artist->id;

            if ($this->Bookmarks->save($Book)) {
                $this->Flash->success("c'est cool, tu aime cet artiste");
                return $this->redirect(['controller'=>'Artists', 'action' => 'view', $Book->artist_id]);
            }
            $this->Flash->error("oupsi ca n'a pas marché :/");
        }
    }
}
