<?php

namespace App\Controller;

use App\Model\Entity\Comments;
use Cake\Http\Exception\NotFoundException;

class ArtistsController extends AppController {

    // cett fonction permet d'afficher les artistes trié par ordre alphabetique
     public function index(){
    	$artists = $this->Artists->find()->order('psodonym');
        $this->set(compact('artists'));
     }

     // Cette fonction permet d'afficher un artistes avec les commentaires qui vont avec.
     public function view($id){
         	$one = $this->Artists->get($id, [
                'contain' => ['Albums', 'Bookmarks.Users']

            ]);
         	$this->set(['one'=>$one]);
     }

     // cette fonction permet de pouvoir changer la photo de profile d'un artiste
     // todo mettre le check admin
     public function editpicture($id){
             $artists = $this->Artists->get($id);
             $oldFile = $artists->picture;

             if($this->request->is(['post', 'put'])){
                 $this->Artists->patchEntity($artists, $this->request->getData());

                 if(!empty($this->request->getData()['picture']['name'])
                 && in_array($this->request->getData()['picture']['type'], ['image/png', 'image/jpg', 'image/jpeg', 'image/gif'])){
                     $ext = pathinfo($this->request->getData()['picture']['name'], PATHINFO_EXTENSION);
                     $name = 'a-'.rand(0,3000).'-'.time().'.'.$ext;
                     $address = WWW_ROOT.'/data/artistes/'.$name;
                     $artists->picture = $name;
                     move_uploaded_file($this->request->getData('picture')['tmp_name'],$address);

                     if($this->Artists->save($artists)){

                         if(!empty($oldFile) && file_exists(WWW_ROOT . '/data/artistes/' . $oldFile))
                             unlink(WWW_ROOT . '/data/artistes/' . $oldFile);

                         $this->Flash->success('cette modif est faite');
                         return $this->redirect(['action' => 'view', $artists->id]);
                     }
                 }else{
                     $artists->picture= $oldFile;
                     $this->Flash->error("ce format de donnée est pas bon frère");
                 }
             }
             $this->set(compact('artists'));
     }

     // Cette fonction permet de pouvoir editer un artiste sauf l'image
     // todo voir pour mettre la fonction de changement d'image dedans.
     public function edit($id){
             $artists = $this->Artists->get($id);

             if($this->request->is(['post', 'put'])){
                 $this->Artists->patchEntity($artists, $this->request->getData());

     			if($this->Artists->save($artists)){
     				$this->Flash->success('Modif ok');
     				return $this->redirect(['action' => 'view', $artists->id]);
     			}
                  $this->Flash->error('la modif a planté tu peux essayer encore');
             }
             $this->set(compact('artists'));
     }

     // Cette focntion permet d'ajouter un artiste
    public function add() {
            $new = $this->Artists->newEntity();
            if($this->request->is('post')){
                $new = $this->Artists->patchEntity($new, $this->request->getData());

                if(in_array($this->request->getData()['picture']['type'], array('image/png','image/jpg', 'image/jpeg', 'image/gif'))){
                    $ext = pathinfo($this->request->getData('picture')['name'], PATHINFO_EXTENSION);
                    $name = 'a-'.rand(0,3000).'-'.time().'.'.$ext;
                    $adress=WWW_ROOT.'/data/artistes/'.$name;
                    $new->picture = $name;
                    move_uploaded_file($this->request->getData('picture')['tmp_name'],$adress);
                }else {
                    $new->picture = null;
                    $this->Flash->error('tu peux encore essayer');
                }
                if ($this->Artists->save($new)){
                    $this->Flash->success('jour de fête tu as reussi');
                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error('si tu vois ce message, c\' que tu es mauvais');
            }
            $this->set(compact('new'));
        }
}
