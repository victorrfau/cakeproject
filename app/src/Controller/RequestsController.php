<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

// todo ajouter le traitement des demandes

use App\Model\Entity\Albums;
use Cake\Http\Exception\NotFoundException;


class RequestsController extends AppController{

    public function index(){
        $requests = $this->Requests->find()->order('created');
        $this->set(compact('requests'));
    }
    public function add(){
        $requests = $this->Requests->newEntity();

        if ($this->request->is('post')) {
            $requests = $this->Requests->patchEntity($requests, $this->request->getData());
            if ($this->Requests->save($requests)) {
                $this->Flash->success("on va voir ce qu'on peux faire mais on te promet rien faut pas déconer");
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error('Try again');
        }
        $this->set(compact('requests'));
    }

}
