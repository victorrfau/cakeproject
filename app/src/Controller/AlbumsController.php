<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use App\Model\Entity\Albums;
use Cake\Http\Exception\NotFoundException;


class AlbumsController extends AppController{

    public function viewAlbum($id){
        $albums = $this->Albums->get($id);
        $this->set(compact('albums'));
    }
    public function editpicture($id){
        $albums = $this->Albums->get($id);

        $oldFile = $albums->picture;


        if($this->request->is(['post', 'put'])){
            $this->Albums->patchEntity($albums, $this->request->getData());

            if(!empty($this->request->getData()['cover']['name']) && in_array($this->request->getData()['cover']['type'], ['image/png', 'image/jpg', 'image/jpeg', 'image/gif'])){

                $ext = pathinfo($this->request->getData()['cover']['name'], PATHINFO_EXTENSION);
                $name = 'a-'.rand(0,3000).'-'.time().'.'.$ext;
                $address = WWW_ROOT.'/data/pochettes/'.$name;
                $albums->cover = $name;
                move_uploaded_file($this->request->getData('cover')['tmp_name'],$address);

                if($this->Albums->save($albums)){



                    if(!empty($oldFile) && file_exists(WWW_ROOT . '/data/pochettes/' . $oldFile))
                        unlink(WWW_ROOT . '/data/pochettes/' . $oldFile);


                    $this->Flash->success('Modif ok');
                    return $this->redirect(['action' => 'viewAlbum', $albums->id]);
                }

            }else{
                $albums->cover= $oldFile;
                $this->Flash->error('Ce format de fichier n\'est pas autorisé');

            }
        }
        $this->set(compact('albums'));
    }
    public function edit($id){
        $albums = $this->Albums->get($id);

        if($this->request->is(['post', 'put'])){
            $this->Albums->patchEntity($albums, $this->request->getData());
            if($this->Albums->save($albums)){
                $this->Flash->success('Modif ok');
                return $this->redirect(['action' => 'viewAlbum', $albums->id]);
            }
            $this->Flash->error('Modif plantée');
        }
        $this->set(compact('albums'));
    }
}
