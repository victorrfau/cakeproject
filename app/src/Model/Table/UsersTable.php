<?php //fichier /src/Model/Table/UsersTable.php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class UsersTable extends Table{

    public function initialize(array $config){
        $this->addBehavior('Timestamp');

        $this->hasMany('Requests',[
            'foreingKey' => 'user_id',
        ]);
        $this->hasMany('Bookmarks',[
             'foreingKey' => 'user_id',
        ]);
    }

    public function validationDefault(Validator $v){
         $v->maxLength('username', 50)
            ->notEmpty('username')
            ->notEmpty('password');
        return $v;
    }

}
