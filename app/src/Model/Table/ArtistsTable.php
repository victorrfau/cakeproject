<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class ArtistsTable extends Table{

    public function initialize(array $config) {
        $this->addBehavior('Timestamp');


        $this->hasMany('Albums',[
            'foreingKey' => 'artist_id',
        ]);
        $this->hasMany('Bookmarks',[
            'foreingKey' => 'artist_id',
        ]);

    }

    public function validationDefault(Validator $v){
        $v->notEmpty('psodonym')
            ->allowEmpty('debut')
            ->allowEmpty('country')
            ->maxLength('country', 100)
            ->allowEmpty('spotify')
            ->maxLength('spotify', 300)
            ->allowEmpty('picture')
            ->maxLength('picture', 100);
        return $v;
    }

}
