<?php
// src/Model/Table/MoviesTable.php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class AlbumsTable extends Table{

    public function initialize(array $config){
        $this->addBehavior('Timestamp');

        $this->belongsTo('artists',[
            'foreingKey' => 'artist_id',
            'joinType' => 'INNER'
        ]);


    }

    public function validationDefault(Validator $v){
        $v->notEmpty('content')
            ->notEmpty('grade');
        return $v;
    }
}
