<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class RequestsTable extends Table{

    public function initialize(array $config) {
        $this->addBehavior('Timestamp');

        $this->belongsTo('users',[
            'foreingKey' => 'user_id',
            'joinType' => 'INNER'
        ]);


    }

    public function validationDefault(Validator $v){
        $v->notEmpty('artistname')
            ->maxLength('artistname', 100)
            ->notEmpty('albumtitle')
            ->maxLength('spotify', 100);
        return $v;
    }

}
