<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class BookmarksTable extends Table{

    public function initialize(array $config) {
        $this->addBehavior('Timestamp');

        $this->belongsTo('Artists',[
            'foreingKey' => 'artist_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users',[
            'foreingKey' => 'user_id',
            'joinType' => 'INNER'
        ]);

    }


}
